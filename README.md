# A simple template for phaser3 games

Only for simple testing on diffrent OS by beginers, do not requires nodejs. But for most cases [phaser3 examples](https://github.com/photonstorm/phaser3-examples) and [phaser3 project template](https://github.com/photonstorm/phaser3-project-template) should be better. 

## Komma igång

Få tag på ett exempel av detta arkiv, antingen genom att klona det:

```bash
git clone https://gitlab.com/krmit/easy-phaser3-template.git
```

Eller ladda ned en komprimerad fil([zip](http://bit.ly/htsit-p3)), packa upp filen.


Har du git installerad kan du alltid updatera genom terminal kommandot:

```bash
git pull
```

## Användning

För att utveckla ett litet exempel kan du göra på följande sätt.

1. Börja med att starta den lilla webserver som kommer med. Exemplen nedan startar en webserv er på port 8080.
  * **Linux:** ./bin/ran-linux -l -p 8080
  * **Windows:** ./bin/ran-win -l -p 8080 _Inte testat_
  * **Mac:** ./bin/ran-mac -l -p 8080 _Inte testat, kanske kräver att "darwin" är installerad_

**⚠** _Detta kräver att du har rätt att köra en körbarfil på din dator._

2. Gå till mappen "my-code".

3. Kopiera filen "tempalte.html" till vad du vill att ditt demo ska heta. Filnamnet ska sluta på ".html".

4. Öppna ditt demo med din favorit editor.

5. Mellan script tagarna där det står "// Your code" skriver du din kod. Du kan men några mindre motifikationer inkludera exempel från phaser 3. Tänk på:
  * Det finns en "assert" mapp som är en underkatalog till detta projekt. Du kan nå denna genom att skriva "../assert" i början på dina sökvägar.
  * Att alla funktioner som används i "scene" som hittas i objectet "config" måste defineras i koden eller tas bort.

6. Öppna din webläsare på följande adress: [http://localhost:8080](http://localhost:8080). Klicka dig frma till ditt demo.

7. Ta och försöka komma på vad som kan vara fel. Du kan:
  * Trycka **F5** för att ladda om ditt demo och se dina ändringa på nytt.
  * Trycka **F12** för att få se en debug konsol. Här kan du bland anna se utskrifter från metoden _console.log()_. 
  * Trycka **alt-tab** för att snabbt byta till din editor.
  
8. Gå till editorn och ändra ding kod.

9. Bli väldigt glad när du gör något som ser kul ut!

Lycka till!
